const { response } = require("express");

const printDrinkMiddleware = (request, response, next) => {
    console.log("Course URL: " + request.url);
    console.log(response);
    next();
}

module.exports = {
    printDrinkMiddleware: printDrinkMiddleware
}