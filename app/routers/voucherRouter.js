//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import Drink Middleware
const { printVoucherMiddleware } = require("../middlewares/voucherMiddleware");

//Import module controllers
const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controllers/voucherController');
const voucherRouter = express.Router();

//Get a voucher
voucherRouter.get("/vouchers", printVoucherMiddleware, getAllVoucher);

//Create a voucher
voucherRouter.post("/vouchers", printVoucherMiddleware, createVoucher);

//Get a voucher by id
voucherRouter.get("/vouchers/:voucherId", printVoucherMiddleware, getVoucherById);

//Update a voucher by id
voucherRouter.put("/vouchers/:voucherId", printVoucherMiddleware, updateVoucherById);

//Delete a voucher
voucherRouter.delete("/vouchers/:voucherId", printVoucherMiddleware, deleteVoucherById);

//Export dữ liệu thành 1 module 
module.exports = voucherRouter;