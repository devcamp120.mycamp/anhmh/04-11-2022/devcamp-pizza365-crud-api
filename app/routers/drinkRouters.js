//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import Drink Middleware
const { printDrinkMiddleware } = require("../middlewares/drinkMiddleware");

//Import module controllers
const { getAllDrink, getDrinkById, createDrink, updateDrinkById, deleteDrinkById } = require('../controllers/drinkController');
const drinkRouter = express.Router();

//Get a drink
drinkRouter.get("/drinks", printDrinkMiddleware, getAllDrink);

//Create a drink
drinkRouter.post("/drinks", printDrinkMiddleware, createDrink);

//Get a drink by id
drinkRouter.get("/drinks/:drinkId", printDrinkMiddleware, getDrinkById);

//Update a drink by id
drinkRouter.put("/drinks/:drinkId", printDrinkMiddleware, updateDrinkById);

//Delete a drink
drinkRouter.delete("/drinks/:drinkId", printDrinkMiddleware, deleteDrinkById);

//Export dữ liệu thành 1 module 
module.exports = drinkRouter;
