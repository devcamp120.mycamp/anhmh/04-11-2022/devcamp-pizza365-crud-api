const mongoose = require("mongoose");

const userModel = require("../models/userModel");
const orderModel = require("../models/orderModel");
const { response, request } = require("express");

//Create a user
const createUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[fullName] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    if (!requestBody.address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[address] is required!"
        })
    }
    if (!requestBody.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[phone] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newUserInput = {
        _id: mongoose.Types.ObjectId(),
        fullName: requestBody.fullName,
        email: requestBody.email,
        address: requestBody.address,
        phone: requestBody.phone,
        orders: mongoose.Types.ObjectId()
    }
    console.log(newUserInput)
    userModel.create(newUserInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create User Success",
                data: data
            })
        }
    })
}


//Get all user
const getAllUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All User",
                data: data
            })
        }
    })
}


//Limit users
const limitUsers = (request, response) => {
    userModel.find().limit(2).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Success: Limit user",
            data: data
        })
    })
}

//Skip users
const skipUsers = (request, response) => {
    userModel.find().skip(3).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Success: Skip user",
            data: data
        })
    })
}

//Sort users
const sortUsers = (request, response) => {
    userModel.find().sort({ fullName: 'asc' }).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Success: Sort user",
            data: data
        })
    })
}

//Skip limit users
const skipLimitUsers = (request, response) => {
    userModel.find().skip(0).limit(1).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Success: Skip Limit User",
            data: data
        })
    })
}
//Sort skip limit users
const sortSkipLimitUsers = (request, response) => {
    userModel.find().sort({ fullName: 'asc' }).skip(0).limit(1).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Success: Sort Skip Limit User",
            data: data
        })
    })
}
//Get a user by id
const getAllUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        userModel.findById(userId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All User",
                    data: data
                })
            }
        })
    }
}

//Update a user by id
const updateUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            message: "[userId] is invalid!"
        })
    }
    if (!requestBody.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[fullName] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    if (!requestBody.address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[address] is required!"
        })
    }
    if (!requestBody.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[phone] is required!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updateUserInput = {
            fullName: requestBody.fullName,
            email: requestBody.email,
            address: requestBody.address,
            phone: requestBody.phone
        }
        console.log(updateUserInput)
        userModel.findByIdAndUpdate(userId, updateUserInput, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Get All User",
                    data: data
                })
            }
        });
    }
}

//Delete a user by id
const deteleUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        userModel.findByIdAndDelete(userId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete User Success",
                    data: data
                })
            }
        })
    }
}
module.exports = { createUser, getAllUser, getAllUserById, updateUserById, deteleUserById, limitUsers, skipUsers, sortUsers, skipLimitUsers, sortSkipLimitUsers }
